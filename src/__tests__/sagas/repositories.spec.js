import { runSaga } from "redux-saga";
import api from "../../services/api";
import MockAdapter from "axios-mock-adapter";

import { getRepositories } from "../../store/sagas/repositories";
import { Creators as RepositoriesActions } from "../../store/ducks/repositories";

const apiMock = new MockAdapter(api);

describe("Repositories Saga", () => {
  it("should be able to fetch repositories", async () => {
    const dispatched = [];

    apiMock.onGet("users/raianandrades/repos").reply(200, ["Repo 1", "Repo 2"]); //aqui sempre que requisitar a url vamos retornar um 200, e aqui pode passar qualquer coisa, pois não estamos testando reducer nem nada...

    //quando a gente executa o runSaga a gente consegue sobreescrever algumas funções do SAGA. Ele sobreescreve a função do PUT de dentro do SAGA.
    //toda vez que o put for chamado dentro do SAGA ao invés de enviar para o REDUX, ele vai pegar o parametro que estamos enviando que são as actions,
    // para dentro do array dispatched, então vamos conseguie saber quais as actions são disparadas. No final da um .toPromise que ai sim podemos utilizar o await.
    await runSaga(
      {
        dispatch: action => dispatched.push(action)
      },
      getRepositories
    ).toPromise();

    expect(dispatched).toContainEqual(
      RepositoriesActions.getSuccess(["Repo 1", "Repo 2"])
    ); // no caso de getFailure vai falhar
  });
});
